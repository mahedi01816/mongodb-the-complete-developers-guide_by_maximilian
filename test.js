const mongoDB = require('mongodb').MongoClient;

mongoDB.connect('').then(client => {
    console.log("Connected!");
    client.db().collection('test').insertOne({name: "test"});
    client.close();
}).catch(error => {
    console.log(error);
});