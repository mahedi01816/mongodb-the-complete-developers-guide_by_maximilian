db.contacts.aggregate([
    {$match: {gender: "male"}},
    {$project: {_id: 0, name: 1, birthDate: {$toDate: "$registered"}}},
    {$sort: {birthDate: 1}},
    {$skip: 10},
    {$limit: 10},
    {$out: "newCollection"}
]).pretty()

db.contacts.aggregate([
    {$bucketAuto: {
        groupBy: "$age",
            buckets: 5,
            output: {
            numberOfPersons: {$sum: 1},
                averageAge: {$avg: "$age"}
            }
        }}
]).pretty()

db.contacts.aggregate([
    {$bucket: {
        groupBy: "$age",
            boundaries: [18, 30, 40, 50, 60, 80],
            output: {
            numberOfPersons: {$sum: 1},
                averageAge: {$avg: "$age"},
                names: {$push: "$name"}
            }
        }}
]).pretty()

db.contacts.aggregate([
    {$unwind: "$friends"},
    {$project: {_id: 1, name: 1, age: 1, id: "$friends.id"}},
    {$sort: {id: -1}},
    {$group: {_id: "$_id", name: {$first: "$name"}, maxId: {$max: "$id"}}},
    {$sort: {maxId: -1}}
]).pretty()


db.contacts.aggregate([
    {
        $project: {
            _id: 0,
            idss: {$filter: {input: '$friends', as: 'friends', cond: {$gt: ["$$friends.id", 1]}}}
        }
    }
]).pretty()

db.contacts.aggregate([
    {$project: {_id: 0, numScores: {$size: "$friends"}}}
]).pretty()

db.contacts.aggregate([
    {$project: {_id: 0, friends: {$slice: ["$friends", 1]}}}
]).pretty()

db.contacts.aggregate([
    {$unwind: "$tags"},
    {$group: {_id: {age: "$age"}, allHobbies: {$addToSet: "$tags"}}}
]).pretty()

db.contacts.aggregate([
    {
        $project: {
            _id: 0,
            name: 1,
            email: 1,
            birthDate: {$toDate: '$registered'},
            age: "$age",
            location: {
                type: 'Point',
                coordinates: [
                    {
                        $convert: {
                            input: '$longitude',
                            to: 'double',
                            onError: 0.0,
                            onNull: 0.0
                        }
                    },
                    {
                        $convert: {
                            input: '$latitude',
                            to: 'double',
                            onError: 0.0,
                            onNull: 0.0
                        }
                    }
                ]
            }
        }
    },
    {
        $project: {
            gender: 1,
            email: 1,
            location: 1,
            birthDate: 1,
            age: 1,
            fullName: {
                $concat: [
                    {$toUpper: {$substrCP: ['$name', 0, 1]}},
                    {$substrCP: ['$name', 1, {$subtract: [{$strLenCP: "$name"}, 1]}]}]
            }
        }
    },
    {$group: {_id: {birthYear: {$isoWeekYear: "$birthDate"}}, numPersons: {$sum: 1}}},
    {$sort: {numPersons: -1}}
]).pretty()

db.contacts.aggregate([
    {
        $project: {
            _id: 0,
            gender: 1,
            fullTags: {
                $concat: [
                    {$toUpper: {$substrCP: ['$name', 0, 1]}},
                    {$substrCP: ['$name', 1, {$subtract: [{$strLenCP: "$name"}, 1]}]},
                    " ",
                    {$toUpper: {$substrCP: ['$company', 0, 1]}},
                    {$substrCP: ['$company', 1, {$subtract: [{$strLenCP: "$company"}, 1]}]},
                    " ",
                    {$toUpper: {$substrCP: ['$email', 0, 1]}},
                    {$substrCP: ['$email', 1, {$subtract: [{$strLenCP: "$email"}, 1]}]}]
            }
        }
    }
]).pretty()

db.contacts.aggregate([
    {$match: {gender: 'female'}},
    {$group: {_id: {name: "$friends.name"}, totalPersons: {$sum: 1}}},
    {$sort: {totalPersons: -1}}
]).pretty()